FROM: John Dallas, Senior Manager, Security, Bell
REF: https://bellnetwork.slack.com/archives/C01S7Q3GHS7/p1643665739038749?thread_ts=1643310644.031800&cid=C01S7Q3GHS7
TODO: Convert to YAML

Alshmemri, M., Shahwan-Akl, L., & Maude, P. (2017). Herzberg’s two-factor theory. Life Science Journal, 14(5), 12-16.

Baard, P. P., Deci, E. L., & Ryan, R. M. (2004). Intrinsic need satisfaction: a motivational basis of performance and weil-being in two work settings 1. Journal of applied social psychology, 34(10), 2045-2068.

Barbosa, A. C. (2021). Strategies for Motivating and Retaining Millennial Workers.

Bartholomew, K. J., Ntoumanis, N., Ryan, R. M., Bosch, J. A., & Thøgersen-Ntoumani, C. (2011). Self-determination theory and diminished functioning: The role of interpersonal control and psychological need thwarting. Personality and Social Psychology Bulletin, 37(11), 1459-1473.

Bassett-Jones, N., & Lloyd, G. C. (2005). Does Herzberg's motivation theory have staying power? Journal of Management Development, 24(10), 929-943. doi: doi:10.1108/02621710510627064

Blouin, M. (2004). L'influence de la réalisation des promesses faites aux employés sur l'engagement organisationnel : l'effet modérateur du statut d'emploi.

Bontis, N., Richards, D., & Serenko, A. (2011). Improving service delivery: Investigating the role of information sharing, job characteristics, and employee satisfaction. The Learning Organization, 18(3), 239-250. doi: doi:10.1108/09696471111123289

Cerasoli, C. P., Nicklin, J. M., & Ford, M. T. (2014). Intrinsic motivation and extrinsic incentives jointly predict performance: A 40-year meta-analysis. Psychological bulletin, 140(4), 980.

Chiat, L. C., & Panatik, S. A. (2019). Perceptions of Employee Turnover Intention by Herzberg’s Motivation-Hygiene Theory: A Systematic Literature Review. Journal of Research in Psychology, 1(2), 10-15.

Dalmas, M. (2007). Les méta modèles de la motivation au travail: constructions théoriques et propositions de recherche. Les notes du LIRHE, note, (446).

Davis, W. E., Kelley, N. J., Kim, J., Tang, D., & Hicks, J. A. (2016). Motivating the academic mind: High-level construal of academic goals enhances goal meaningfulness, motivation, and self-concordance. Motivation and Emotion, 40(2), 193-202. doi: 10.1007/s11031-015-9522-x

Deci, E. L., Cascio, W. F., & Krusell, J. (1975). Cognitive evaluation theory and some comments on the Calder and Staw critique. Journal of personality and social psychology, 31(1), 81-85.

Deci, E. L., Koestner, R., & Ryan, R. M. (1999). A meta-analytic review of experiments examining the effects of extrinsic rewards on intrinsic motivation. Psychological Bulletin, 125(6), 627-668. doi: 10.1037/0033-2909.125.6.627

Deci, E. L., Olafsen, A. H., & Ryan, R. M. (2017). Self-determination theory in work organizations: The state of a science. Annual Review of Organizational Psychology and Organizational Behavior, 4, 19-43.

Deci, E. L., & Ryan, R. M. (1985). The general causality orientations scale: Self-determination in personality. Journal of research in personality, 19(2), 109-134.

Deci, E. L., & Ryan, R. M. (2000). The" what" and" why" of goal pursuits: Human needs and the self-determination of behavior. Psychological inquiry, 11(4), 227-268.

Dittmar, H., Bond, R., Hurst, M., & Kasser, T. (2014). The relationship between materialism and personal well-being: A meta-analysis. Journal of personality and social psychology, 107(5), 879.

Dweck, C. S. (1986). Motivational processes affecting learning. American psychologist, 41(10), 1040.

Gagné, M., & Deci, E. L. (2005). Self-determination theory and work motivation. Journal of Organizational behavior, 26(4), 331-362.

Gagné, M., & Forest, J. (2008). The study of compensation systems through the lens of self-determination theory: Reconciling 35 years of debate (Vol. 49). Educational Publishing Foundation.

Gagné, M., Forest, J., Gilbert, M.-H., Aubé, C., Morin, E., & Malorni, A. (2010). The motivation at work scale: Validation evidence in two languages. Educational and psychological measurement, 70(4), 628-646.

Gagné, M., Forest, J., Gilbert, M.-H., Aubé, C., Morin, E., & Malorni, A. (2010). The motivation at work scale: Validation evidence in two languages. Educational and psychological measurement, 70(4), 628-646.

Gerhart, B., & Fang, M. (2015). Pay, intrinsic motivation, extrinsic motivation, performance, and creativity in the workplace: Revisiting long-held beliefs. Annu. Rev. Organ. Psychol. Organ. Behav., 2(1), 489-521.

Gerhart, B., Rynes, S. L., & Fulmer, I. S. (2009). 6 pay and performance: individuals, groups, and executives. Academy of Management annals, 3(1), 251-315.

Ghazi, S. R., Shahzada, G., & Khan, M. S. (2013). Resurrecting Herzberg’s two factor theory: An implication to the university teachers. Journal of Educational and Social Research, 3(2), 445.

Ghazzawi, I. (2011). Does age matter in job satisfaction? The case of US information technology professionals. Journal of Organizational Culture, Communications and Conflict, 15(1), 25.

Gosselin, E., & Lauzier, M. (2011). Le présentéisme. Lorsque la présence n´est pas garante de la performance. Revue française de gestion, 37(211), 15-27. doi: 10.3166/rfg.211.15-27

Herzberg, F. M., & Mausner, B. (1959). B. & Snyderman, B.(1959). The motivation to work, 2, 49-58.

Heyman, J., & Ariely, D. (2004). Effort for payment: A tale of two markets. Psychological science, 15(11), 787-793.

Hodgins, H. S., Koestner, R., & Duncan, N. (1996). On the compatibility of autonomy and relatedness. Personality and Social Psychology Bulletin, 22(3), 227-237.

Hogenelst, K., Schelvis, R., Krone, T., Gagné, M., Heino, M. T., Knittle, K. P., & Hankonen, N. (2020). A within-person approach to the relation between quality of task motivation, performance and job satisfaction in everyday working life.

Howard, J. L., Morin, A. J., & Gagné, M. (2020). A longitudinal analysis of motivation profiles at work. Motivation and Emotion, 1-21.

Hur, Y. (2017). Testing Herzberg’s Two-Factor Theory of Motivation in the Public Sector: Is it Applicable to Public Managers? Public Organization Review, 18(3), 329-343. doi: 10.1007/s11115-017-0379-1

Kim, S. (2009). IT Employee Job Satisfaction in the Public Sector. International Journal of Public Administration, 32(12), 1070-1097. doi: 10.1080/01900690903170303

Korsakiene, R., Stankeviciene, A., Šimelyte, A., & Talackiene, M. (2015). Factors driving turnover and retention of information technology professionals. Journal of Business Economics and Management, 16(1), 1-17. doi: 10.3846/16111699.2015.984492

Kulikowski, K., & Sedlak, P. (2020). Can you buy work engagement? the relationship between pay, fringe benefits, financial bonuses and work engagement. Current Psychology, 39(1), 343-353.

Kuvaas, B., Buch, R., & Dysvik, A. (2020). Individual variable pay for performance, controlling effects, and intrinsic motivation. Motivation and Emotion, 1-9.

Kuvaas, B., Buch, R., Weibel, A., Dysvik, A., & Nerstad, C. G. L. (2017). Do intrinsic and extrinsic motivation relate differently to employee outcomes? Journal of Economic Psychology, 61, 244-258. doi: https://doi.org/10.1016/j.joep.2017.05.004

Lam, C. F., & Gurland, S. T. (2008). Self-determined work motivation predicts job outcomes, but what predicts self-determined work motivation? Journal of research in personality, 42(4), 1109-1115.

Litalien, D., Morin, A. J., Gagné, M., Vallerand, R. J., Losier, G. F., & Ryan, R. M. (2017). Evidence of a continuum structure of academic self-determination: A two-study test using a bifactor-ESEM representation of academic motivation. Contemporary Educational Psychology, 51, 67-82.

Locke, E., & Schattke, K. (2019). Intrinsic and extrinsic motivation: Time for expansion and clarification. Motivation Science, 5 (4), 277–290.

Lund, D. B. (2003). Organizational culture and job satisfaction. The Journal of Business & Industrial Marketing, 18(2), 219-234. doi: 10-1108/0885862031047313

Maslow, A. H. (1954). The instinctoid nature of basic needs. Journal of Personality.

McGregor, L., & Doshi, N. (2015). How company culture shapes employee motivation. Harvard Business Review, 11, 1-13.

Melnik, G., & Maurer, F. (2006). Comparative Analysis of Job Satisfaction in Agile and Non-agile Software Development Teams. Berlin, Heidelberg.

Mete, E. S., Sökmen, A., & Biyik, Y. (2016). The relationship between organizational commitment, organizational identification, person-organization fit and job satisfaction: A research on IT employees. International Review of Management and Business Research, 5(3), 870.

Miller, L. A., & Von Hagel, W. (2009). Precipitating events leading to voluntary employee turnover among information technology professionals: A qualitative phenomenological study. : ProQuest Dissertations Publishing.

Milyavskaya, M., & Koestner, R. (2011). Psychological needs, motivation, and well-being: A test of self-determination theory across multiple domains. Personality and Individual Differences, 50(3), 387-391. doi: https://doi.org/10.1016/j.paid.2010.10.029

Mitchell, T. R., Holtom, B. C., & Lee, T. W. (2001). How to keep your best employees: Developing an effective retention policy. Academy of Management Executive, 15(4), 96-108. doi: 10.5465/AME.2001.5897929

Mitchell, T. R., Holtom, B. C., Lee, T. W., Sablynski, C. J., & Erez, M. (2001). WHY PEOPLE STAY: USING JOB EMBEDDEDNESS TO PREDICT VOLUNTARY TURNOVER. Academy of Management Journal, 44(6), 1102-1121. doi: 10.5465/3069391

Murayama, K., Matsumoto, M., Izuma, K., & Matsumoto, K. (2010). Neural basis of the undermining effect of monetary reward on intrinsic motivation. Proceedings of the National Academy of Sciences, 107(49), 20911-20916.

Murayama, K., Matsumoto, M., Izuma, K., Sugiura, A., Ryan, R. M., Deci, E. L., & Matsumoto, K. (2015). How self-determined choice facilitates performance: A key role of the ventromedial prefrontal cortex. Cerebral Cortex, 25(5), 1241-1251.

Niemiec, C. P., Ryan, R. M., & Deci, E. L. (2009). The path taken: Consequences of attaining intrinsic and extrinsic aspirations in post-college life. Journal of research in personality, 43(3), 291-306.

Olafsen, A. H., Halvari, H., Forest, J., & Deci, E. L. (2015). Show them the money? The role of pay, managerial need support, and justice in a self-determination theory model of intrinsic work motivation. Scandinavian journal of psychology, 56(4), 447-457.

Osemeke, M., & Adegboyega, S. (2017). Critical Review and Comparism between Maslow, Herzberg, and McClelland" s Theory of Needs. Funai Journal of Accounting, Business and Finance, 1(1), 161-173.

Parijat, P., & Bagga, S. (2014). Victor Vroom’s expectancy theory of motivation–An evaluation. International Research Journal of Business and Management, 7(9), 1-8.

Patall, E. A., Cooper, H., & Robinson, J. C. (2008). The effects of choice on intrinsic motivation and related outcomes: a meta-analysis of research findings. Psychological bulletin, 134(2), 270.

Rawsthorne, L. J., & Elliot, A. J. (1999). Achievement goals and intrinsic motivation: A meta-analytic review. Personality and Social Psychology Review, 3(4), 326-344.

Reid, M. F., Riemenschneider, C. K., Allen, M. W., & Armstrong, D. J. (2008). Information Technology Employees in State Government:A Study of Affective Organizational Commitment, Job Involvement, and Job Satisfaction. The American Review of Public Administration, 38(1), 41-61. doi: 10.1177/0275074007303136

Ryan, R. M., & Deci, E. L. (2017). Self-determination theory: Basic psychological needs in motivation, development, and wellness. Guilford Publications.

Ryan, R. M., & Deci, E. L. (2017). Self-determination theory: Basic psychological needs in motivation, development, and wellness. Guilford Publications.

Ryan, R. M., & Deci, E. L. (2019). Brick by brick: The origins, development, and future of self-determination theory. Dans Advances in motivation science (Vol. 6,  pp. 111-156): Elsevier.

Ryan, R. M., & Deci, E. L. (2019). Research on intrinsic and extrinsic motivation is alive, well, and reshaping 21st-century management approaches: Brief reply to Locke and Schattke (2019).

Ryan, R. M., & Deci, E. L. (2020). Intrinsic and extrinsic motivation from a self-determination theory perspective: Definitions, theory, practices, and future directions. Contemporary Educational Psychology, 101860.

Ryan, R. M., Deci, E. L., & Vansteenkiste, M. (2016). Autonomy and autonomy disturbances in self-development and psychopathology: Research on motivation, attachment, and clinical process. Developmental psychopathology, 1-54.

Ryan, R. M., Koestner, R., & Deci, E. L. (1991). Ego-involved persistence: When free-choice behavior is not intrinsically motivated. Motivation and emotion, 15(3), 185-205.

Samuel, M. O., & Chipunza, C. (2009). Employee retention and turnover: Using motivational variables as a panacea. African journal of business management, 3(9), 410-415.

Schreiber, J. B. (2016). Motivation 101. Springer Publishing Company.

Schutte, N. S., & Malouff, J. M. (2019). Increasing curiosity through autonomy of choice. Motivation and Emotion, 43(4), 563-570.

Shannon, E. A., BA,GradCert, PhD. (2019). Motivating the workforce: Beyond the 'two-factor' model. Australian Health Review, 43(1), 98-102. doi: doi:http://proxybiblio.uqo.ca:2068/10.1071/AH16279

Sheldon, K. M., & Prentice, M. (2017). Self-determination theory as a foundation for personality researchers. Journal of personality, 87(1), 5-14.

Simpson, E. H., & Balsam, P. D. (2015). The behavioral neuroscience of motivation: an overview of concepts, measures, and translational applications. Dans Behavioral Neuroscience of Motivation (pp. 1-12): Springer.

Slemp, G. R., Kern, M. L., Patrick, K. J., & Ryan, R. M. (2018). Leader autonomy support in the workplace: A meta-analytic review. Motivation and emotion, 42(5), 706-724.

Tessem, B., & Maurer, F. (2007). Job Satisfaction and Motivation in a Large Agile Team. Berlin, Heidelberg.

Ünlü, A. (2016). Adjusting Potentially Confounded Scoring Protocols for Motivation Aggregation in Organismic Integration Theory: An Exemplification with the Relative Autonomy or Self-Determination Index. Frontiers in psychology, 7, 272-272. doi: 10.3389/fpsyg.2016.00272

Vallerand, R. J., Blais, M. R., Lacouture, Y., & Deci, E. L. (1987). L'Échelle des Orientations Générales à la Causalité: Validation canadienne française du General Causality Orientations Scale. [The General Causality Orientations Scale: The Canadian French version of the General Causality Orientations Scale.]. Canadian Journal of Behavioural Science / Revue canadienne des sciences du comportement, 19(1), 1-15. doi: 10.1037/h0079872

Vallerand, R. J., Pelletier, L. G., & Koestner, R. (2008). Reflections on self-determination theory. Canadian Psychology/Psychologie Canadienne, 49(3), 257.

Van den Broeck, A., Howard, J. L., Van Vaerenbergh, Y., Leroy, H., & Gagné, M. (2021). Beyond Intrinsic and Extrinsic Motivation: A Meta-Analysis on Self-Determination Theory’s Multidimensional Conceptualization of Work Motivation. Organizational Psychology Review.

Van Wassenhove, W. (2014). Modèle de Karasek.

Vansteenkiste, M., Ryan, R. M., & Soenens, B. (2020). Basic psychological need theory: Advancements, critical themes, and future directions. : Springer.

Vansteenkiste, M., Simons, J., Lens, W., Sheldon, K. M., & Deci, E. L. (2004). Motivating learning, performance, and persistence: the synergistic effects of intrinsic goal contents and autonomy-supportive contexts. Journal of personality and social psychology, 87(2), 246.

Vroom, V. H., & Deci, E. L. (1971). The stability of post-decision dissonance: A follow-up study of the job attitudes of business school graduates. Organizational Behavior and Human Performance, 6(1), 36-49.

Wang, N., Zhu, J., Dormann, C., Song, Z., & Bakker, A. B. (2019). The Daily Motivators: Positive Work Events, Psychological Needs Satisfaction, and Work Engagement. Applied Psychology, 69(2), 508-537. doi: 10.1111/apps.12182

Weibel, A., Rost, K., & Osterloh, M. (2007). Crowding-out of intrinsic motivation-opening the black box. Available at SSRN 957770.
