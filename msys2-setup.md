# Install MSYS2

1. Install __PortableApps__                from https://portableapps.com/download
1. Install __MSYS2 Portable__              from https://portableapps.com/node/56214
1. Download latest `msys2-x86_64-*.tar.xz` from http://repo.msys2.org/distrib/x86_64
1. Unzip into `PortableApps\MSYS2Portable\App\msys32`
1. Use Windows home directory to allow single config location for multiple apps
	1. https://stackoverflow.com/questions/33942924/how-to-change-home-directory-and-start-directory-on-msys2/44351945#44351945
	1. Edit `PortableApps\MSYS2Portable\App\msys32\etc\nsswitch.conf` to
		replace `db_home: cygwin desc`
		with    `db_home: env windows /c/Users/%U`
	1. `sed -ibak 's!^\(db_home:\).*$!\1 /c/User/%U!' '/etc/nsswitch.conf'`

# Update MSYS2

1. Launch `MSYS2Portable.exe`
	1. See __Packages__ for the difference between `msys2`, `mingw64` and `mingw32`.
		https://www.msys2.org/wiki/How-does-MSYS2-differ-from-Cygwin
1. Follow instructions for __Updating packages__ https://www.msys2.org/wiki/MSYS2-installation

# Install Apps

```sh
pacman -Sy git "${MINGW_PACKAGE_PREFIX}-helix"
```

# Helix

1. Install Markdown Language Server
	 1. Download Windows binary from https://github.com/artempyanykh/marksman/releases
	 2. Move `marksman.exe` to `%LOCALAPPDATA\Microsoft\WindowsApps\`

# Ruby

1. Install `chruby`.
	```sh
	cd "${TMP:-/tmp}"
	wget -O chruby-0.3.9.tar.gz https://github.com/postmodern/chruby/archive/v0.3.9.tar.gz
	tar -xzvf chruby-0.3.9.tar.gz
	cd chruby-0.3.9/
	./scripts/setup.sh # => /usr/local/share/chruby/
	```
1. Install `ruby-install`.
	```sh
	wget -O ruby-install-0.8.5.tar.gz https://github.com/postmodern/ruby-install/archive/v0.8.5.tar.gz
	tar -xzvf ruby-install-0.8.5.tar.gz
	cd ruby-install-0.8.5/
	make install
	```
1.  Install GCC.
	```sh
	pacman -S mingw-w64-ucrt-x86_64-gcc
	```
1.  Install Ruby.
	```sh
	ruby-install ruby 2.7
1.  Install Solargraph Language Server	```
