# OpenSSH - Windows (MYSYS2), Mac, Linux, Android (Termux)

Read https://stribika.github.io/2015/01/04/secure-secure-shell.html.

1. Create keys with `ssh-keygen` (one-time)
   1. Run the following jist
      ```sh
      ssh-keygen -t ed25519     -a 100 -C "$(id -un)@$(hostname)"
      # if dealing with very old systems that only support RSA, then
      ssh-keygen -t rsa -b 4096 -a 100 -C "$(id -un)@$(hostname)"
      ```
      > __Tip:__ `-C "$(id -un)`@`$(hostname)"` allows us to identify
      > who the key belongs to
   1. When prompted, enter a __key passphrase__.
      You will need to enter this whenever you reboot.
      > __Tip:__ Use a Password Manager, such as [KeePass](https://keepass.info),
      > to generate and store your passphrases.
1. Setup the key agent `ssh-agent` (one-time)
   1. The following script will setup `ssh-agent` for reuse across multiple
      shells, (so you don’t have to keep inputting the long, random passphrase
      to unlock your ssh keys whenever you open a new terminal).
      ```sh
      git clone https://github.com/go2null/sshag.git /tmp/sshag
      sh /tmp/sshag/sshag.sh install
      ```
1. Add your key to the agent with `ssh-add` (on every boot)
   1. Add your keys to the `ssh-agent`. Enter your passphrase when prompted,
      ```sh
      ssh-add
      ```
1. Copy your public key (once per target server)
   * GitLab: https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account
   * `ssh-copy-id`: If you have password access to the REMOTE_SERVER,
      then use the following to copy your public key (e.g., `id_ed25519.pub`) over.
      ```sh
      ssh-copy-id -i "$HOME/.ssh/id_ed25519" "<REMOTE_USER>@<REMOTE_SERVER>"
      ```
      > __Note:__ Replace `<REMOTE_USER>` and `<REMOTE_SERVER>` with the
      > actual values.

# PuTTY - Windows

Watch https://www.youtube.com/watch?v=2nkAQ9M6ZF8.

1. Create keys with `PUTTYGEN`
   1. Use `PUTTYGEN.EXE` in the PuTTY directory with the parameters from above
      * RSA:     4096 bits
      * ED25519: nothing to configure
   1. __Generate__.
   1. Prepend `<NAME>`PuTTY-@ to the __Key comment__ field
   1. When prompted, enter a __key passphrase__.
      You will need to enter this whenever you reboot.
      > __Tip:__ Use a Password Manager, such as [KeePass](https://keepass.info),
      > to generate and store your passphrases.
1. Setup the key agent `PAGENT`
   1. Setup `PAGENT.EXE` to autoload your *private_keys* for passwordless
      login to the servers.

# Resources

1. https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair
