# Short flow

* :eyes:           = `NEW`    - _Aware and Acknowledged_
* :scales:         = `TRIAGE` - _Triage_
* :octagonal_sign: = `WAIT`   - _Waiting/Blocked_
* :hourglass:      = `WIP`    - _In-progress_
* :checkered_flag: = `DONE`   - _Completed_


# Detailed flow

| Status | Icon                                                          | Field                           | Check  | Description                   |
| ---    | ---                                                           | ---                             | ---    | ---                           |
| TRIAGE | :scales:                      `:scales:`                      |                                 | Weekly | Funnel                        |
| PLAN   | :map:                         `:map:`                         |                                 | Weekly | Backlog                       |
| REFINE | :alembic:                     `:alembic:`                     |                                 | Weekly |                               |
| TODO   | :pushpin:                     `:pushpin:`                     | `Assignee`, `To Do`, `@mention` | Daily  |                               |
| WAIT   | :octagonal_sign:              `:octagonal_sign:`              |                                 | Daily  | waiting (generic/undefined)   |
| * LINK | :link:                        `:link:`                        |                                 | Weekly | waiting on another issue/epic |
| * HALT | :raised_hand:                 `:raised_hand:`                 |                                 | Daily  | waiting on someone            |
| * HOLD | :egg:                         `:egg:`                         |                                 | Weekly | waiting for an event          |
| * DATE | :date:                        `:date:`                        |                                 | Weekly | waiting for a specific date   |
| WIP    | :hourglass:                   `:hourglass:`                   |                                 | Daily  |                               |
| DONE   | :heavy_check_mark:            `:heavy_check_mark:`            | `Closed`                        | Sprint | Completed                     |
| CLOSED | ::negative_squared_cross_mark `:negative_squared_cross_mark:` | `Closed`                        | Sprint | Closed, Not Done              |
| ---    | ---                                                           | ---                             | ---    | ---                           |
| INFO   | :eyes:                        `:eyes:`                        |                                 | Weekly | FYI, monitor, watch           |


# Resources

1. GitLab supported emojis:        https://github.com/yodamad/gitlab-emoji
1. GitHub supported emojis API:    https://api.github.com/emojis
1. GitHub supported emojis picker: https://github-emoji-picker.rickstaa.dev/
1. Commit message emojis:          https://gitmoji.dev
